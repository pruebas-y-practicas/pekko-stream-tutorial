ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.12"

lazy val root = (project in file("."))
  .settings(
    name := "pekkoStreamTutorial"
  )
  .settings(
    libraryDependencies += "org.apache.pekko" %% "pekko-stream" % "1.0.2"
  )
