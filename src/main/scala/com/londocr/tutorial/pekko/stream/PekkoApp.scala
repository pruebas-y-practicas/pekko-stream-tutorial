package com.londocr.tutorial.pekko.stream

import org.apache.pekko.actor.ActorSystem

import scala.concurrent.{ExecutionContextExecutor, Future}

trait PekkoApp extends App {
  protected val actorSystemName: String
  protected implicit lazy val system: ActorSystem = ActorSystem(actorSystemName)
  protected implicit lazy val ec: ExecutionContextExecutor = system.dispatcher

  def validarParaTerminar(futures: Future[_]*): Unit = {
    Future.traverse(futures)(identity)
      .onComplete(_ => system.terminate())
  }

}
