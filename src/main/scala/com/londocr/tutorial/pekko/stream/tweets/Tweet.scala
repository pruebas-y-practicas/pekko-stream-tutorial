package com.londocr.tutorial.pekko.stream.tweets

final case class Tweet(author: Author, timestamp: Long, body: String) {
  def hashtags: Set[Hashtag] =
    body
      .split(" ")
      .collect {
        case t if t.startsWith("#") => Hashtag(t.replaceAll("[^#\\w]", ""))
      }
      .toSet
}

final case class Author(handle: String)

final case class Hashtag(name: String)
