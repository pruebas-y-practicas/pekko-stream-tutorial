package com.londocr.tutorial.pekko.stream

import org.apache.pekko
import pekko.stream._
import pekko.stream.scaladsl._
import pekko.{ Done, NotUsed }
import pekko.util.ByteString
import scala.concurrent._
import scala.concurrent.duration._
import java.nio.file.Paths

object Main extends PekkoApp {

  override protected val actorSystemName: String = "QuickStart"

  private val source: Source[Int, NotUsed] = Source(1 to 100)
  private val done: Future[Done] = source.runForeach(i => println(i))

  private val factorials = source.scan(BigInt(1))((acc, next) => acc * next)

  val result: Future[IOResult] =
    factorials.map(num => ByteString(s"$num\n")).runWith(FileIO.toPath(Paths.get("factorials.txt")))

  private val result2 = factorials.map(_.toString).runWith(lineSink("factorial2.txt"))

  private val zipping = factorials
    .zipWith(Source(0 to 100))((num, idx) => s"$idx! = $num")
    .throttle(1, 1.second)
    .runForeach(println)

  validarParaTerminar(done, result, result2, zipping)

  def lineSink(filename: String): Sink[String, Future[IOResult]] =
    Flow[String].map(s => ByteString(s + "\n")).toMat(FileIO.toPath(Paths.get(filename)))(Keep.right)
}
