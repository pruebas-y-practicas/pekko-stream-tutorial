package com.londocr.tutorial.pekko.stream.tweets

import com.londocr.tutorial.pekko.stream.PekkoApp
import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl._

object TweetTest extends PekkoApp {

  val pekkoTag = Hashtag("#pekko")

  val tweets: Source[Tweet, NotUsed] = Source{
    Seq(
      Tweet(Author("rolandkuhn"), System.currentTimeMillis, "#pekko rocks!"),
      Tweet(Author("patriknw"), System.currentTimeMillis, "#pekko !"),
      Tweet(Author("bantonsson"), System.currentTimeMillis, "#pekko !"),
      Tweet(Author("drewhk"), System.currentTimeMillis, "#pekko !"),
      Tweet(Author("ktosopl"), System.currentTimeMillis, "#pekko on the rocks!"),
      Tweet(Author("mmartynas"), System.currentTimeMillis, "wow #pekko !"),
      Tweet(Author("pekkoteam"), System.currentTimeMillis, "#pekko rocks!"),
      Tweet(Author("bananaman"), System.currentTimeMillis, "#bananas rock!"),
      Tweet(Author("appleman"), System.currentTimeMillis, "#apples rock!"),
      Tweet(Author("drama"), System.currentTimeMillis, "we compared #apples to #oranges!")
    )
  }


  override protected val actorSystemName: String = "reactive-tweets"

  val result = tweets
    .filterNot(_.hashtags.contains(pekkoTag)) // Remove all tweets containing #pekko hashtag
    .map(_.hashtags) // Get all sets of hashtags ...
    .reduce(_ ++ _) // ... and reduce them to a single set, removing duplicates across all tweets
    .mapConcat(identity) // Flatten the set of hashtags to a stream of hashtags
    .map(_.name.toUpperCase) // Convert all hashtags to upper case
    .runWith(Sink.foreach(println)) // Attach the Flow to a Sink that will finally print the hashtags

  validarParaTerminar(result)

}
